package LibraryProject;


public class LibraryObject {
    private String name;
    private String libraryItemType;
    
    private boolean available;


    public LibraryObject(String name, String libraryItemType) {
        this.name = name;
        this.libraryItemType = libraryItemType;
        available = true;
    }

    @Override
    public String toString() {
        return "Library item type: " + libraryItemType + " Name: " + name + " Available: " + available;
    }

    public String getName() {
        return name;
    }

    public String getLibraryItemType() {
        return libraryItemType;
    }

    public boolean getAvailability() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}