package LibraryProject;

import java.util.Optional;

public class LibraryMain {
    public static void main(String args[]) {
        LibraryTerminal libraryTerminal = new LibraryTerminal();
        libraryTerminal.run();
    }
}
