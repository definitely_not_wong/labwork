public class Accounts {
    
    private String name;

    private double balance;

    static double interestRate = .23;

    public Accounts(String initialName, double initialValue) {
        name = initialName;
        balance = initialValue;
    }

    public Accounts() {
        name = "Me";
        balance = 50.0;
    }

    static double getInterestRate () {
        return interestRate;
    }

    static void setInterestRate(double newInterestRate) {
        interestRate = newInterestRate;
    }

    public void addInterest() {}

    public boolean withdraw() {
        return withdraw(20.0);
    }

    public boolean withdraw(double amount) {
        if (amount > balance) {
            return false;
        }

        balance -= amount;
        return true;
    }

    public void setBalance(double newBalance) {
        balance = newBalance;
    }

    public double getBalance() {
        return balance;
    }
}