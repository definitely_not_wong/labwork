public class CurrentAccount extends Accounts {
    public CurrentAccount (String name, double balance) {
        super(name, balance);
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.1);
        System.out.println("Current");
    }

    public void current() {
        System.out.println("Current");
    }
}