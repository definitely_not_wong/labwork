public class SavingAccount extends Accounts {
    public SavingAccount (String name, double balance) {
        super(name, balance);
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.4);
        System.out.println("Saving");
    }

    public void savingAccount() {
        System.out.println("Saving");
    }
}