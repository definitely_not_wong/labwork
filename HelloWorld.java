public class HelloWorld {
    public static void main(String[] args) {
        Accounts acc1 = new Accounts();
        Accounts acc2 = new SavingAccount("saving", 23.0);
        Accounts acc3 = new CurrentAccount("current", 23.0);

        SavingAccount sav = new SavingAccount("Saving", 23.0);

        acc1.addInterest();
        acc2.addInterest();
        acc3.addInterest();
    }
}