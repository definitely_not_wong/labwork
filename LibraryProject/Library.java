package LibraryProject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;


public class Library {
    private  HashMap<String, ArrayList<LibraryObject>> libraryInvetory;

    private static String CANNOT_BORROW = "periodicals";
    private static int MAX_ITEM_COUNT = 6;
    private static String[] INITIAL_LIBRARY_ITEM_TYPES = new String[]{"book", "cd", "dvd", "periodicals"};

    public Library() {
        libraryInvetory = new HashMap<String, ArrayList<LibraryObject>>();

        for( String libraryItemType : INITIAL_LIBRARY_ITEM_TYPES) {
            libraryInvetory.put(libraryItemType, new ArrayList<LibraryObject>());
        }
    }

    public boolean addLibraryItem( LibraryObject newLibraryItem) {
        if (libraryInvetory.get(newLibraryItem.getLibraryItemType()).size() >= MAX_ITEM_COUNT) {
            return false;
        }

        return libraryInvetory.get(newLibraryItem.getLibraryItemType()).add(newLibraryItem);
    }

    public Optional<LibraryObject> removeLibraryItem( String libraryItemType,  String libraryItemName) {
        ArrayList<LibraryObject> libraryItems = libraryInvetory.get(libraryItemType);

        for (int i = 0; i < libraryItems.size(); i++) {
            if (libraryItems.get(i).getName().equals(libraryItemName)) {
                return Optional.of(libraryItems.remove(i));
            }
        }
        return Optional.empty();
    }

    public boolean returnLibraryItem(String libraryItemType,  String libraryItemName) {
        Optional<LibraryObject> foundItem = getLibraryItem(libraryItemType, libraryItemName);

        if (foundItem.isEmpty()) {
            return false;
        }

        LibraryObject libraryItem = foundItem.get();

        if (!libraryItem.getAvailability()) {
            libraryItem.setAvailable(true);
            return true;
        }
        return false;
    }

    public boolean borrowLibraryItem( String libraryItemType,  String libraryItemName) {
        if (libraryItemName.equals(CANNOT_BORROW)) {
            return false;
        }

        Optional<LibraryObject> foundItem = getLibraryItem(libraryItemType, libraryItemName);

        if (foundItem.isEmpty()) {
            return false;
        }

        LibraryObject libraryItem = foundItem.get();

        if (libraryItem.getAvailability()) {
            libraryItem.setAvailable(false);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String output = "Library:\n";
        for( String libraryItemType : libraryInvetory.keySet()) {
            output += String.format("\t %s: ", libraryItemType);
            for ( LibraryObject libraryItem : libraryInvetory.get(libraryItemType)) {
                output += String.format("%s, ", libraryItem);
            }
            output += "\n";
        }

        return output;
    }

    private Optional<LibraryObject> getLibraryItem(String libraryItemType, String libraryItemName) {
        ArrayList<LibraryObject> libraryItems = libraryInvetory.get(libraryItemType);

        if (libraryItems == null) {
            System.out.println(libraryItemType + " does not exist in the libraries inventory");
            return Optional.empty();
        }

        for (LibraryObject libraryItem : libraryItems) {
            if (libraryItem.getName().equals(libraryItemName)) {
                return Optional.of(libraryItem);
            }
        }
        System.out.println(libraryItemName + " does not exist in the libraries inventory");
        return Optional.empty();
    }
}